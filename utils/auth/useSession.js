import { useState, useEffect } from 'react'
import { usePouch } from 'use-pouchdb'
import PouchDB from 'pouchdb'
// https://github.com/pouchdb-community/pouchdb-authentication/blob/master/docs/api.md#dbsignupusername-password--options--callback
import Authentication from 'pouchdb-authentication'

import Router from 'next/router'

const sessionStates = {
    loading: 0,
    loggedIn: 1,
    loggedOut: 2,
}

const dbBaseUrl = new URL('https://couch.codea.me/')
const sessionUrl = new URL('./_session', dbBaseUrl)
const usersDB = new URL('./_users', dbBaseUrl)

export default function useSession() {
    const db = usePouch() // for syncing
    const remoteDb = new PouchDB('https://couch.codea.me/', { skip_setup: true })
    PouchDB.plugin(Authentication)

    const [sessionState, setSessionState] = useState(sessionStates.loading)
    const [userMeta, setUserMeta] = useState({})
    const [username, setUsername] = useState('')
    const [timeout, setTimeout] = useState(true)

    // const [roles, setRoles] = useState([])

    const checkSessionState = () => {
        console.log('Checking session...')
        remoteDb
            .getSession({ credentials: 'include' })
            .then(function (response) {
                if (!response.userCtx.name) {
                    // nobody's logged in
                    console.error('No hay usuario:', response)
                    setSessionState(sessionStates.loggedOut)
                    setUsername('')
                    console.log('No user, go signin/signup page...')
                    Router.push('/signin')
                } else {
                    console.log('OK, Sesion:', response)
                    setSessionState(sessionStates.loggedIn)
                    setUsername(response.userCtx.name)
                    console.log('Logged in ', response.userCtx.name)
                    console.log('Route:', Router.route)
                    if (Router.route == '/signin') {
                        Router.push('/')
                    }
                }
            })
            .catch(function (error) {
                // handle error
                if (error) {
                    // network error
                    console.log('Error al obtener sesion:', error)
                    setSessionState(sessionStates.loggedOut)
                    setUsername('')
                    console.log('No user, go signin/signup page...')
                    Router.push('/signin')
                }
            })
    }

    useEffect(() => {
        // Setup the timeout..
        const timer = setInterval(() => {
            setTimeout((oldTimeout) => !oldTimeout)
        }, 60000 * 2.5)

        // clean the timer
        return () => {
            clearInterval(timer)
        }
    }, [])

    // on timeout, checkSession...
    useEffect(() => {
        if (timeout) {
            console.log('Timeout!')
            checkSessionState()
        } else console.log('Not timeout...')
    }, [timeout])

    useEffect(() => {
        // Get user metadata
        if (username && sessionState == sessionStates.loggedIn) {
            // console.log(`Getting user metadata para ${username}|${sessionState}...`)
            remoteDb.getUser(username, (err, response) => {
                if (err) {
                    if (err.name === 'not_found') {
                        // typo, or you don't have the privileges to see this user
                        console.log('ERROR al obtener metadatos El usuario no existe:', err.name)
                    } else {
                        // some other error
                        console.log('ERROR al obtener metadatos:', err.name)
                    }
                } else {
                    // response is the user object
                    const meta = {
                        id: response._id,
                        avatar: response.avatar,
                        email: response.email,
                        nombre: response.nombre,
                        roles: response.roles,
                    }
                    setUserMeta(meta)
                }
            })
        }

        // sync effect
        if (sessionState === sessionStates.loggedIn) {
            // whenever we are logged in: start syncing
            console.log('Syncing local <-> remote Database')

            // Get the URL of the users remote db
            const remoteDbUrl = new URL(getUserDatabaseName(username), dbBaseUrl)
            // And sync
            const sync = db
                .sync(remoteDbUrl.href, {
                    retry: true,
                    live: true,
                })
                .on('change', function (info) {
                    // handle change
                    console.log('Hubo un cambio:', info)
                })
                .on('paused', function (err) {
                    // replication paused (e.g. replication up to date, user went offline)
                    console.log('Hubo una interrupcion:', err)
                })
                .on('active', function () {
                    // replicate resumed (e.g. new changes replicating, user went back online)
                    console.log('Resumed...')
                })
                .on('denied', function (err) {
                    // a document failed to replicate (e.g. due to permissions)
                    console.error('Error de permisos', err)
                })
                .on('complete', function (info) {
                    // handle complete
                    console.log('Se completo:', info)
                })
                .on('error', function (err) {
                    // handle error
                    console.log('Hubo un error:', err)
                    setSessionState(sessionStates.loggedOut)
                    setUsername('')
                    sync.cancel()
                    Router.push('/signin')
                    console.log('Error en sync:', err)
                })

            console.log(`${remoteDbUrl} <-> local`)
            return () => {
                // and cancel syncing whenever our sessionState changes
                sync.cancel()
            }
        }
    }, [sessionState, username, db])

    return {
        sessionState: sessionState,
        user: username,
        userMetadata: userMeta,
        localDb: db,
    }
}

function getUserDatabaseName(name, prefix = 'userdb-') {
    const encoder = new TextEncoder()
    const buffy = encoder.encode(name)
    const bytes = Array.from(buffy).map((byte) => byte.toString(16).padStart(2, '0'))
    if (name) {
        return prefix + bytes.join('')
    } else return ''
}
