import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import MenuItem from '@material-ui/core/MenuItem'
import InputAdornment from '@material-ui/core/InputAdornment'
import Select from '@material-ui/core/Select'
import Chip from '@material-ui/core/Chip'
import Paper from '@material-ui/core/Paper'
import DialogTitle from '@material-ui/core/DialogTitle'
import PeopleAltIcon from '@material-ui/icons/PeopleAlt'

import { useSnackbar } from 'notistack'
import { useState, useEffect } from 'react'
import { useForm, Controller } from 'react-hook-form'

import { makeStyles, useTheme } from '@material-ui/core/styles'

import { md5 } from 'hash-wasm'

const isEmpty = (obj) => {
    // console.log('Empty:', Object.keys(obj).length === 0, 'keys:', Object.keys(obj))
    return Object.keys(obj).length === 0
}

const getMd5 = async (str) => {
    return await md5(str)
}

const objectIdFromDate = (str) => {
    return getMd5(str).then((hash_) => {
        return (Math.floor(new Date() / 1000).toString(16) + hash_.toString()).slice(0, 32).toUpperCase()
    })
}

const DialogoAgregarProyecto = (props) => {
    const { setOpenDialog, opened, localDb, user, customers } = props
    const { enqueueSnackbar } = useSnackbar()
    const { handleSubmit, control, formState, reset, register } = useForm({ mode: 'all', shouldFocusError: true })
    // https://github.com/react-hook-form/react-hook-form/discussions/2357

    const [cleared, setCleared] = useState(false)
    const [habilitada, setHabilitada] = useState(false)
    const [generalError, setGeneralError] = useState('')
    const [saving, setSaving] = useState(false)
    const [miembros, setMiembros] = useState('')

    useEffect(() => {
        // console.log('AgregarProy > formState errors:', formState.errors)
        // console.log('AgregarProy > valid', formState.isValid)
        setHabilitada(formState.isValid)
    }, [formState, miembros])

    const onSubmit = async (data) => {
        setSaving(true)
        // save on localDb
        const id = await objectIdFromDate(data.name)
        const doc = {
            _id: id,
            model: 'projects',
            name: data.name,
            members: miembros, // trick: this data is not in the form, because it did not work!
            customer: data.customer,
            fee: data.fee,
            logoUrl: data.logoUrl,
            created_at: new Date().toJSON(),
            created_by: user.email,
        }
        console.log('Doc:', doc)

        const result = await localDb.put(doc)
        console.log('Resultado de Agregar a BD:', result)

        if (result.ok) {
            // ok todo salio bien!
            setSaving(false)
            setOpenDialog(false)
            // reset form
            reset()
            setMiembros([])
            enqueueSnackbar(`Se agrego el proyecto ${doc.name}`)
        } else {
            // Hubo error
            console.error('Error al agregar cliente:', result)
            enqueueSnackbar(`Error al agregar el proyecto ${doc.name}: ${result}`)
        }
    }

    const availMembers = [
        'Oliver Hansen',
        'Van Henry',
        'April Tucker',
        'Ralph Hubbard',
        'Omar Alexander',
        'Carlos Abbott',
        'Miriam Wagner',
        'Bradley Wilkerson',
        'Virginia Andrews',
        'Kelly Snyder',
    ]

    function getStyles(name, personName, theme) {
        return {
            fontWeight:
                personName.indexOf(name) === -1 ? theme.typography.fontWeightRegular : theme.typography.fontWeightMedium,
        }
    }

    const ITEM_HEIGHT = 48
    const ITEM_PADDING_TOP = 8
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    }

    const useStyles = makeStyles((theme) => ({
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        chips: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        chip: {
            margin: 2,
        },
        noLabel: {
            marginTop: theme.spacing(3),
        },
    }))

    const theme = useTheme()
    const classes = useStyles()

    return (
        <Dialog
            open={opened}
            onClose={() => {
                setOpenDialog(false)
                console.log('Reseting from...')
                setMiembros([])
                reset()
            }}
            aria-labelledby='form-dialog-title'>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
                <DialogTitle id='form-dialog-title'>Agregar Proyecto</DialogTitle>
                <DialogContent>
                    {saving && <DialogContentText>Guardando...</DialogContentText>}
                    <FormControl className={classes.formControl} fullWidth>
                        <Controller
                            name='name'
                            as={
                                <TextField
                                    autoFocus
                                    id='name'
                                    label='Nombre del Proyecto'
                                    size='small'
                                    aria-label='Nombre del proyecto'
                                    helperText={formState.errors.name ? formState.errors.name.message : null}
                                    error={formState.errors.name ? true : false}
                                    className={formState.errors.name ? 'error' : 'no-error'}
                                />
                            }
                            control={control}
                            defaultValue=''
                            rules={{
                                required: 'Requerido',
                            }}
                        />
                    </FormControl>
                    <FormControl className={classes.formControl} fullWidth>
                        <Controller
                            name='customer'
                            control={control}
                            defaultValue=''
                            rules={{
                                required: 'Requerido',
                            }}
                            as={
                                <TextField
                                    select
                                    id='customer'
                                    label='Cliente'
                                    size='small'
                                    aria-label='Cliente'
                                    helperText={formState.errors.customer ? formState.errors.customer.message : null}
                                    error={!isEmpty(formState.errors) && formState.errors.customer ? true : false}
                                    fullWidth
                                    className={formState.errors.customer ? 'error' : 'no-error'}>
                                    {customers.map((customer) => (
                                        <MenuItem key={customer._id} value={customer._id}>
                                            {customer.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            }
                        />
                    </FormControl>
                    <FormControl className={classes.formControl} fullWidth>
                        <InputLabel htmlFor='teamMembers'>Team members</InputLabel>
                        <Select
                            multiple
                            id='teamMembers'
                            labelId='teamMembers'
                            value={miembros || []}
                            onChange={(event) => {
                                console.log('toAdd:', event.target.value)
                                setMiembros(event.target.value)
                            }}
                            input={<Input id='select-teamMembers' />}
                            MenuProps={MenuProps}
                            renderValue={(selected) => (
                                <div className={classes.chips}>
                                    {selected.map((value) => (
                                        <Chip
                                            className={classes.chip}
                                            key={value}
                                            label={value}
                                            icon={<PeopleAltIcon />}
                                            onMouseDown={(event) => {
                                                // Needed because the select intercepts the mousedown events....
                                                event.stopPropagation()
                                            }}
                                            onDelete={() => {
                                                setMiembros((chips) => chips.filter((chip) => chip !== value))
                                            }}
                                        />
                                    ))}
                                </div>
                            )}>
                            {availMembers.map((name) => (
                                <MenuItem key={name} value={name} style={getStyles(name, availMembers, theme)}>
                                    {name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl} fullWidth>
                        <Controller
                            name='logoUrl'
                            as={
                                <TextField
                                    id='logoUrl'
                                    label='URL del Logotipo (opcional)'
                                    size='small'
                                    aria-label='logotipo (opcional)'
                                    helperText={formState.errors.logoUrl ? formState.errors.logoUrl.message : null}
                                    error={formState.errors.logoUrl ? true : false}
                                    fullWidth
                                    className={formState.errors.logoUrl ? 'error' : 'no-error'}
                                />
                            }
                            control={control}
                            defaultValue=''
                            rules={{
                                required: false,
                            }}
                        />
                    </FormControl>
                    <FormControl className={classes.formControl} fullWidth>
                        <Controller
                            name='fee'
                            as={
                                <TextField
                                    id='fee'
                                    type='number'
                                    label='Tarifa por hora'
                                    size='small'
                                    aria-label='tarifa por hora del proyecto'
                                    helperText={formState.errors.fee ? formState.errors.fee.message : null}
                                    error={formState.errors.fee ? true : false}
                                    className={formState.errors.fee ? 'error' : 'no-error'}
                                />
                            }
                            control={control}
                            defaultValue={0.0}
                            rules={{
                                required: false,
                            }}
                        />
                    </FormControl>
                    {generalError && <div>{generalError}</div>}
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={() => {
                            setOpenDialog(false)
                            // do nothing!
                        }}
                        color='secondary'>
                        Cancelar
                    </Button>

                    <Button type='submit' color='primary' variant='contained' disabled={!habilitada}>
                        Agregar
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
}

const DialogoAgregarCliente = (props) => {
    const { setOpenDialog, opened, localDb, user } = props
    const { handleSubmit, control, formState, reset } = useForm({ mode: 'all', shouldFocusError: true })
    const { enqueueSnackbar } = useSnackbar()
    const [habilitada, setHabilitada] = useState(false)

    const [generalError, setGeneralError] = useState('')
    const [saving, setSaving] = useState(false)

    useEffect(() => {
        // console.log('AgregarCliente > formState:', formState)
        // console.log('AgregarCliente > isValid', formState.isValid)
        setHabilitada(formState.isValid)
    }, [formState])

    const onSubmit = async (data) => {
        setSaving(true)
        // save on localDb
        const id = await objectIdFromDate(data.email)
        const doc = {
            _id: id,
            model: 'customers',
            email: data.email,
            name: data.name,
            urlAvatar: data.urlAvatar,
            created_at: new Date().toJSON(),
            created_by: user.email,
        }

        const result = await localDb.put(doc)
        console.log('Resultado de Agregar a BD:', result)

        if (result.ok) {
            // ok todo salio bien!
            setSaving(false)
            setOpenDialog(false)
            // reset form
            reset()
            enqueueSnackbar(`Se agrego el cliente ${doc.name}`)
        } else {
            // Hubo error
            console.error('Error al agregar cliente:', result)
            enqueueSnackbar(`Error al agregar el proyecto ${doc.name}: ${result}`)
        }
    }

    return (
        <Dialog
            open={opened}
            onClose={() => {
                setOpenDialog(false)
                reset()
            }}
            aria-labelledby='form-dialog-title'>
            <form onSubmit={handleSubmit(onSubmit)} noValidate>
                <DialogTitle id='form-dialog-title'>Agregar Cliente</DialogTitle>
                <DialogContent>
                    {saving && <DialogContentText>Guardando...</DialogContentText>}

                    <FormControl fullWidth>
                        <Controller
                            name='name'
                            as={
                                <TextField
                                    autoFocus
                                    id='name'
                                    label='Nombre del Cliente'
                                    size='small'
                                    aria-label='Nombre del cliente'
                                    helperText={formState.errors.name ? formState.errors.name.message : null}
                                    error={formState.errors.name ? true : false}
                                    className={formState.errors.name ? 'error' : 'no-error'}
                                />
                            }
                            control={control}
                            defaultValue=''
                            rules={{
                                required: 'Requerido',
                            }}
                        />
                    </FormControl>
                    <FormControl fullWidth>
                        <Controller
                            name='email'
                            as={
                                <TextField
                                    id='email'
                                    label='Email del Cliente'
                                    size='small'
                                    aria-label='email del cliente'
                                    helperText={formState.errors.email ? formState.errors.email.message : null}
                                    error={formState.errors.email ? true : false}
                                    className={formState.errors.email ? 'error' : 'no-error'}
                                />
                            }
                            control={control}
                            defaultValue=''
                            rules={{
                                required: 'Requerido',
                                pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                    message: 'Email inválido',
                                },
                            }}
                        />
                    </FormControl>
                    <FormControl fullWidth>
                        <Controller
                            name='urlAvatar'
                            as={
                                <TextField
                                    id='urlAvatar'
                                    label='Foto ó logotipo (opcional)'
                                    size='small'
                                    aria-label='Foto ó logotipo (opcional)'
                                    helperText={formState.errors.urlAvatar ? formState.errors.urlAvatar.message : null}
                                    error={formState.errors.urlAvatar ? true : false}
                                    fullWidth
                                    className={formState.errors.urlAvatar ? 'error' : 'no-error'}
                                />
                            }
                            control={control}
                            defaultValue=''
                            rules={{
                                required: false,
                            }}
                        />
                    </FormControl>
                    {generalError && <div>{generalError}</div>}
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={() => {
                            setOpenDialog(false)
                            // do nothing!
                        }}
                        color='secondary'>
                        Cancelar
                    </Button>
                    <Button type='submit' color='primary' variant='contained' disabled={!habilitada}>
                        Agregar
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    )
}

export { DialogoAgregarCliente, DialogoAgregarProyecto }
