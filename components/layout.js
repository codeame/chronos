import Head from 'next/head'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

import Link from '@material-ui/core/Link'
import MenuIcon from '@material-ui/icons/Menu'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import NotificationsIcon from '@material-ui/icons/Notifications'
import Brightness3Icon from '@material-ui/icons/Brightness3'
import WbSunnyIcon from '@material-ui/icons/WbSunny'
import Drawer from '@material-ui/core/Drawer'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Badge from '@material-ui/core/Badge'
import Container from '@material-ui/core/Container'
import AboutIcon from '@material-ui/icons/Info'
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter'
import AssignmentIcon from '@material-ui/icons/Assignment'
import PeopleAltIcon from '@material-ui/icons/PeopleAlt'
import List from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ListSubheader from '@material-ui/core/ListSubheader'
import Switch from '@material-ui/core/Switch'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Avatar from '@material-ui/core/Avatar'
import Divider from '@material-ui/core/Divider'
import { withStyles } from '@material-ui/core/styles'
import IconButton from '@material-ui/core/IconButton'

import { useFind } from 'use-pouchdb'
import { useRouter } from 'next/router'
import { useState, useEffect } from 'react'

import { temaChronosLight, temaChronosDark } from '../style/temaChronos'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import React from 'react'

import { motion } from 'framer-motion'
import { AnimateFadeIn } from '../utils/motion-variants'

// https://github.com/mui-org/material-ui/blob/master/docs/src/pages/getting-started/templates/dashboard/Dashboard.js

const drawerWidth = 240
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    flexcontainer: {
        display: 'flex',
        flexFlow: 'row nowrap!important',
        alignItems: 'center',
        fontSize: '1rem',
        cursor: 'pointer',
    },
    appName: {
        marginLeft: 10,
        fontWeight: 300,
    },
    iconoApp: {
        flex: '0 1 auto',
        margin: '0px',
        padding: 0,
        width: '40px !important',
        height: '40px !important',
    },
    iconoNoche: {
        color: '#424242',
        fontSize: '1.25rem',
        padding: '0.1rem',
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.24),0px 1px 3px 0px rgba(0,0,0,0.22)',
        // boxShadow: '0px 2px 1px -1px rgba(255,255,255,0.2),0px 1px 1px 0px rgba(255,255,255,0.14),0px 1px 3px 0px rgba(255,255,255,0.12)',
        borderRadius: '50%',
    },
    iconoDia: {
        backgroundColor: theme.palette.warning.main,
        color: '#424242',
        fontSize: '1.25rem',
        padding: '0.1rem',
        // backgroundColor: 'rgba(0, 0, 0, 0.5)',
        boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.24),0px 1px 3px 0px rgba(0,0,0,0.22)',
        borderRadius: '50%',
    },
    appBar: {
        backgroundColor: theme.palette.primary.main,
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
    cajaUsuario: {
        display: 'flex',
        marginBottom: 20,
    },
    cajaDatos: {
        flexFlow: 'column',
        marginLeft: 5,
        marginRight: 10,
        textAlign: 'left',
    },
    avatar: {
        width: 55,
        height: 55,
        margin: 'auto',
    },
    nombre: {
        fontSize: 14,
        color: theme.palette.grey[600],
        marginBottom: '1px',
        marginTop: 0,
    },
    email: {
        fontSize: 11,
        color: theme.palette.grey[500],
        marginTop: '0px',
        marginBottom: '1px',
    },
    botonSignout: {
        maxHeight: 20,
        fontSize: '0.65rem',
        padding: '4px 6px',
        margin: 0,
        backgroundColor: theme.palette.warning.light,
        '&:hover': {
            backgroundColor: 'rgb(255, 155, 9)',
        },
    },
}))

function Copyright() {
    return (
        <Typography variant='body2' color='textSecondary' align='center'>
            {'Copyright © '}
            {new Date().getFullYear()}
            <Link color='inherit' href='https://chronos.codea.me/' target='_blank'>
                &nbsp;Miguel Chavez, codea.me
            </Link>{' '}
            {'.'}
        </Typography>
    )
}

const Layout = ({ children, titulo, user, userMeta, signout, localDb, toggleDarkMode, darkMode, setDarkMode }) => {
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)')
    const tema = prefersDarkMode ? temaChronosDark : temaChronosLight

    const router = useRouter()
    const [open, setOpen] = useState(true)

    const handleDrawerOpen = () => {
        setOpen(true)
    }
    const handleDrawerClose = () => {
        setOpen(false)
    }
    const classes = useStyles()
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight)

    const YellowSwitch = withStyles({
        switchBase: {
            // color: tema.palette.grey[600],
            '&$checked': {
                color: tema.palette.warning.main,
            },
            '&$checked + $track': {
                backgroundColor: tema.palette.warning.light,
            },
            '&$thumb': {
                color: 'red',
                backgroundColor: 'yellow',
            },
        },

        checked: {},
        track: {},
        thumb: {},
    })(Switch)

    const { docs: config, loading: loadingConfig, done: doneConfig } = useFind({
        // Create and query an index for all Projects
        index: {
            fields: ['model'],
        },
        selector: {
            model: 'user-config',
        },
    })

    useEffect(() => {
        console.log('Layout >> Cambio la configuracion: ', config)
        // config: Array of objects that contain the requested documents
        if (config.length >= 1) {
            // Solo deberia haber un resultado.
            if (darkMode == config[0].darkMode) console.log('DarkMode NOT CHANGED from:', config[0].darkMode)
            else console.log('DarkMode CHANGED to:', config[0].darkMode)
            setDarkMode(config[0].darkMode)
        } else {
            console.log('Layout >> Config size is 0!')
        }
    }, [config])

    const saveConfig = (newMode) => {
        return localDb
            .get('config_chronos')
            .then((doc) => {
                // ok it exists, update it.
                doc.darkMode = newMode
                // setDarkMode(newMode)
                console.log('Layout >> Saving config:', doc)
                // save it
                return localDb.put(doc)
            })
            .catch((error) => {
                // No existe? Crearlo
                if (error.status == 404) {
                    const newDoc = {
                        _id: 'config_chronos',
                        model: 'user-config',
                        darkMode: newMode,
                    }
                    return localDb.put(newDoc)
                }
            })
    }

    if (
        typeof user === 'undefined' ||
        typeof signout === 'undefined' ||
        typeof userMeta === 'undefined' ||
        !user ||
        typeof config === 'undefined' ||
        config.length <= 0
    )
        return null
    return (
        <>
            <div className={classes.root}>
                <Head>
                    <title>Chronos</title>
                    <meta charSet='utf-8' />
                    <meta name='description' content='A Time keeper for the freelancer' />
                    <meta
                        name='viewport'
                        content='minimun-scale=1, maximum-scale=1, initial-scale=1.0, user-scalable=no, width=device-width'
                    />
                    <meta httpEquiv='X-UA-Compatible' content='IE=edge' />

                    <link rel='manifest' href='/manifest.json' />
                    <link rel='icon' href='/icons/chronos-32.png' />

                    <link href='/icons/favicon-16.png' rel='icon' type='image/png' sizes='16x16' />
                    <link href='/icons/favicon-32.png' rel='icon' type='image/png' sizes='32x32' />

                    <link rel='apple-touch-icon' href='/icons/chronos-144.png'></link>
                    <link rel='apple-touch-startup-icon' href='/icons/chronos-144.png' />
                    <meta name='theme-color' content={tema.palette.primary.main} />
                    <meta name='apple-mobile-web-app-capable' content='yes' />
                    <meta name='apple-mobile-web-app-status-bar-style' content='black' />
                    <meta name='apple-mobile-web-app-title' content='Chronos' />
                </Head>
                <CssBaseline />

                <AppBar position='absolute' className={clsx(classes.appBar, open && classes.appBarShift)}>
                    <Toolbar className={classes.toolbar}>
                        <IconButton
                            edge='start'
                            color='inherit'
                            aria-label='open drawer'
                            onClick={handleDrawerOpen}
                            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}>
                            <MenuIcon />
                        </IconButton>
                        <Typography component='h1' variant='h6' color='inherit' noWrap className={classes.title}>
                            {titulo}
                        </Typography>
                        {/* <IconButton color='inherit'>
                            <Badge badgeContent={1} color='secondary'>
                                <NotificationsIcon />
                            </Badge>
                        </IconButton> */}
                    </Toolbar>
                </AppBar>
                <Drawer
                    variant='permanent'
                    classes={{
                        paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
                    }}
                    open={open}>
                    <div className={classes.toolbarIcon}>
                        <Typography component='h1' variant='h6' color='inherit' noWrap className={classes.title}>
                            <div
                                className={classes.flexcontainer}
                                onClick={(e) => {
                                    e.preventDefault()
                                    router.push('/', '/')
                                }}>
                                <img className={classes.iconoApp} src='/cronometro.png' />
                                <h4 className={classes.appName}>Chronos</h4>
                            </div>
                        </Typography>
                        <IconButton onClick={handleDrawerClose}>
                            <ChevronLeftIcon />
                        </IconButton>
                    </div>
                    <Divider />
                    <List>
                        <Box className={classes.cajaUsuario}>
                            <Box className={classes.cajaDatos}>
                                <motion.div
                                    positionTransition
                                    initial={AnimateFadeIn.initial}
                                    animate={AnimateFadeIn.animate}
                                    exit={AnimateFadeIn.exit}>
                                    <Avatar
                                        className={classes.avatar}
                                        src={userMeta?.avatar || 'https://i.pravatar.cc/128'}
                                    />
                                    <Box>
                                        <YellowSwitch
                                            size='medium'
                                            checked={!darkMode}
                                            icon={<Brightness3Icon className={classes.iconoNoche} />}
                                            checkedIcon={<WbSunnyIcon className={classes.iconoDia} />}
                                            onChange={() => {
                                                // save config with the new mode, which is the actual mode toggled.
                                                saveConfig(!darkMode).then((result) => {
                                                    console.log('Layout >> Resultado de guardar config:', result)
                                                })
                                            }}
                                        />
                                    </Box>
                                </motion.div>
                            </Box>
                            <Box classNames={classes.cajaUsuario}>
                                <Box className={classes.cajaDatos}>
                                    <p className={classes.nombre}>{userMeta?.nombre}</p>
                                </Box>
                                <Box className={classes.cajaDatos}>
                                    <p className={classes.email}> {userMeta.email}</p>
                                </Box>
                                <Box className={classes.cajaDatos}>
                                    <Button
                                        size='small'
                                        variant='contained'
                                        className={classes.botonSignout}
                                        onClick={() => {
                                            signout.logout().then(() => {
                                                localDb.destroy().then(() => {
                                                    console.log('Db destroyed!')
                                                })
                                                router.push('/signin')
                                            })
                                        }}>
                                        Cerrar Sesión
                                    </Button>
                                </Box>
                            </Box>
                        </Box>
                        <Divider />
                        <ListSubheader inset>Colecciones</ListSubheader>
                        <ListItem
                            button
                            onClick={(e) => {
                                router.push('/clientes', '/clientes')
                            }}>
                            <ListItemIcon
                                onClick={(e) => {
                                    router.push('/clientes', '/clientes')
                                }}>
                                <PeopleAltIcon />
                            </ListItemIcon>
                            <ListItemText primary='Clientes' />
                        </ListItem>
                        <ListItem
                            button
                            onClick={(e) => {
                                router.push('/proyectos', '/proyectos')
                            }}>
                            <ListItemIcon
                                onClick={(e) => {
                                    router.push('/proyectos', '/proyectos')
                                }}>
                                <BusinessCenterIcon />
                            </ListItemIcon>
                            <ListItemText primary='Proyectos' />
                        </ListItem>
                        <ListItem
                            button
                            onClick={(e) => {
                                router.push('/tareas', '/tareas')
                            }}>
                            <ListItemIcon
                                button
                                onClick={(e) => {
                                    router.push('/tareas', '/tareas')
                                }}>
                                <AssignmentIcon />
                            </ListItemIcon>
                            <ListItemText primary='Tareas' />
                        </ListItem>
                    </List>
                    {/* Contenido de menu: components/menuItems.js */}
                    <Divider />
                    <List>
                        {/* {SecondaryListItems} */}
                        <ListItem button>
                            <ListItemIcon>
                                <AboutIcon />
                            </ListItemIcon>
                            <ListItemText primary='Acera de Chronos' />
                        </ListItem>
                    </List>
                    {/* Fin Contenido de menu */}
                </Drawer>
                {/* Fin del drawer */}

                {/* Contenido principal */}
                <main className={classes.content}>
                    <div className={classes.appBarSpacer} />
                    <Container maxWidth='xl' className={classes.container}>
                        {children}
                        <Box pt={4}>
                            <Copyright />
                        </Box>
                    </Container>
                </main>
            </div>
        </>
    )
}

export default Layout
