import React from 'react'
import { useForm } from 'react-hook-form'

import Container from '@material-ui/core/Container'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import InputAdornment from '@material-ui/core/InputAdornment'
import Chip from '@material-ui/core/Chip'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

import TagFacesIcon from '@material-ui/icons/TagFaces'

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1.7),
        },
    },
    margin: {
        margin: theme.spacing(1.7),
    },
    chipList: {
        display: 'inline-flex',
        flexWrap: 'wrap',
        listStyle: 'none',
        padding: theme.spacing(0.5),
        margin: theme.spacing(1.5),
    },
    chip: {
        margin: theme.spacing(0.5),
    },
}))

const ProjectForm = () => {
    const classes = useStyles()
    const { register, handleSubmit, errors } = useForm()
    console.log(errors)

    const customers = [
        {
            nombre: 'Colegio Anahuac Secundaria',
            _id: 'CACS219201!928EEIFSLC',
        },
        {
            nombre: 'Colegio Anahuac Prepa',
            _id: 'CACP19201!928EEIFSLC',
        },
    ]
    const [customer, setCustomer] = React.useState('CACP19201!928EEIFSLC')
    const handleCustomerChange = (event) => {
        setCustomer(event.target.value)
    }

    const [chipData, setChipData] = React.useState([
        { key: 0, label: 'Angular' },
        { key: 1, label: 'jQuery' },
        { key: 2, label: 'Polymer' },
        { key: 3, label: 'React' },
        { key: 4, label: 'Vue.js' },
    ])

    const handleDelete = (chipToDelete) => () => {
        setChipData((chips) => chips.filter((chip) => chip.key !== chipToDelete.key))
    }

    const onSubmit = (data) => {
        console.log(data)
    }

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)} className={classes.root}>
                <Container>
                    <TextField
                        id='name'
                        name='name'
                        placeholder='Project Name'
                        label='Project Name'
                        size='small'
                        aria-label='project name'
                        error={errors.name ? true : false}
                        className={errors.name ? 'error' : 'no-error'}
                        ref={register({ required: true, maxLength: 128 })}
                    />
                    {errors.name && <Form.Text muted>This field is required</Form.Text>}
                </Container>
                <Container>
                    <TextField
                        id='customer'
                        name='customer'
                        select
                        label='Customer'
                        value={customer}
                        onChange={handleCustomerChange}
                        helperText='Please select the project customer'
                        className={errors.customer ? 'error' : 'no-error'}
                        aria-label='customer'
                        ref={register({ required: true })}>
                        {customers.map((customer) => (
                            <MenuItem key={customer._id} value={customer._id}>
                                {customer.nombre}
                            </MenuItem>
                        ))}
                    </TextField>
                    {errors.customer && <Form.Text muted>This field is required</Form.Text>}
                </Container>
                <Container>
                    <FormControl fullWidth className={classes.margin}>
                        <InputLabel htmlFor='fee'>Hourly fee</InputLabel>
                        <Input
                            id='fee'
                            name='fee'
                            placeholder='5.00'
                            aria-label='hourly fee'
                            startAdornment={<InputAdornment position='start'>$</InputAdornment>}
                            error={errors.fee ? true : false}
                            ref={register({ required: true })}
                            className={errors.fee ? 'error' : 'no-error'}
                        />
                    </FormControl>
                    {errors.fee && <Form.Text muted>This field is required</Form.Text>}
                </Container>
                <Container>
                    <FormControl>
                        <InputLabel htmlFor='teamMembers'>Team members</InputLabel>
                        <Paper component='ul' className={classes.chipList}>
                            {chipData.map((data) => {
                                let icon

                                if (data.label === 'React') {
                                    icon = <TagFacesIcon />
                                }

                                return (
                                    <li key={data.key}>
                                        <Chip
                                            id={data.key}
                                            name={data.key}
                                            icon={icon}
                                            label={data.label}
                                            size='small'
                                            onDelete={data.label === 'React' ? undefined : handleDelete(data)}
                                            className={classes.chip}
                                        />
                                    </li>
                                )
                            })}
                        </Paper>
                    </FormControl>
                    {errors.teamMembers && <Form.Text muted>This field is required</Form.Text>}
                </Container>
                <Container>
                    <Button type='submit'>Save</Button>
                </Container>
            </form>
        </>
    )
}

export default ProjectForm
