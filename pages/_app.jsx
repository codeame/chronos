import '../style/global.scss'

import PouchDB from 'pouchdb'
//  pouchdb-browser causa error al hacer SSR, con dynamic import NO funciona: PouchDB is not a constructor
import { useState, useEffect } from 'react'
import { Provider } from 'use-pouchdb'
import { SnackbarProvider } from 'notistack'
import { ThemeProvider } from '@material-ui/core/styles'
import { temaChronosLight, temaChronosDark } from '../style/temaChronos'
import useDarkMode from 'use-dark-mode'
import { withStyles } from '@material-ui/core/styles'
import React from 'react'
import Switch from '@material-ui/core/Switch'

export default function MyApp({ Component, pageProps, router }) {
    const [db, setDB] = useState(() => new PouchDB('local'))
    const { value: isDark } = useDarkMode(true)
    const [darkMode, setDarkMode] = useState(isDark) // default mode based on useDarkMode.
    const [tema, setTema] = useState(temaChronosLight)

    const YellowSwitch = withStyles({
        switchBase: {
            color: tema.palette.grey[600],
            '&$checked': {
                color: tema.palette.warning.main,
            },
            '&$checked + $track': {
                backgroundColor: tema.palette.warning.light,
            },
        },
        checked: {},
        track: {},
    })(Switch)

    const readConfig = () => {
        return db
            .get('config_chronos')
            .then((doc) => {
                console.log('_APP_ >> Got config:', doc)
                setDarkMode(doc.darkMode)
                return doc
            })
            .catch((error) => {
                // No existe? Crearlo
                console.log('No hay configuracion...')
                return {}
            })
    }

    const saveConfig = (newMode) => {
        return db
            .get('config_chronos')
            .then((doc) => {
                // ok it exists, update it.
                doc.darkMode = newMode
                // save it
                return db.put(doc)
            })
            .catch((error) => {
                // No existe? Crearlo
                if (error.status == 404) {
                    const newDoc = {
                        _id: 'config_chronos',
                        model: 'user-config',
                        darkMode: newMode,
                    }
                    return db.put(newDoc)
                }
            })
    }

    const toggleComponent = (
        <YellowSwitch
            checked={!darkMode}
            onChange={() => {
                setDarkMode(!darkMode)
                // save config.
                saveConfig(!darkMode).then((result) => {
                    console.log('Resultado de guardar config:', result)
                })
            }}
        />
    )

    // change theme when mode changes (via toggle on the Appbar)
    useEffect(() => {
        console.log('darkMode Changed to:', darkMode)
        setTema(darkMode ? temaChronosDark : temaChronosLight)
    }, [darkMode])

    useEffect(() => {
        const listener = (dbName) => {
            if (dbName === 'local') {
                setDB(new PouchDB('local'))
                console.log('Setting local db')
            }
        }
        readConfig()

        PouchDB.on('destroyed', listener)
        return () => {
            PouchDB.removeListener('destroyed', listener)
        }
    }, [])

    return (
        <>
            <Provider pouchdb={db}>
                <ThemeProvider theme={tema}>
                    <SnackbarProvider maxSnack={6}>
                        <Component
                            {...pageProps}
                            key={router.route}
                            toggleDarkMode={toggleComponent}
                            darkMode={darkMode}
                            setDarkMode={setDarkMode}
                        />
                    </SnackbarProvider>
                </ThemeProvider>
            </Provider>
        </>
    )
}
