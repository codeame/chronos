// Nextjs & React
import { useEffect, useState } from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'

// Material UI
import CssBaseline from '@material-ui/core/CssBaseline'
import { makeStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Divider from '@material-ui/core/Divider'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Switch from '@material-ui/core/Switch'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'

// Animations
import { motion } from 'framer-motion'
import { AnimateUp } from '../utils/motion-variants'

// PouchDb
import PouchDB from 'pouchdb'
import Authentication from 'pouchdb-authentication'
// import useSession from '../utils/auth/useSession'

// The signin page needs to be light.. for now.
import { ThemeProvider } from '@material-ui/core/styles'
import { temaChronosLight } from '../style/temaChronos'

// Style
const useStyles = makeStyles((theme) => ({
    root: {
        padding: '12px',
        flexFlow: 'column wrap',
        alignItems: 'center',
        maxWidth: '400px',
        minWidth: '350px',
        borderRadius: '20px',
        backgroundColor: 'white',
        backgroundImage: 'url("/cronometro_opacidad20.png")',
        backgroundSize: '60%',
        backgroundOrigin: 'padding-box',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        boxShadow: '0 10px 25px rgba(0, 0, 0, 0.4)',
        display: 'flex',
    },
    centrado: {
        display: 'flex',
        flexFlow: 'column wrap',
        alignItems: 'center',
        width: '100vw',
    },
    titulo: {
        fontSize: '2.5rem',
        fontWeight: 200,
        marginTop: 4,
        marginBottom: 0,
        fontFamily:
            '"Fira Sans Extra Condensed","-apple-system", "system-ui", "BlinkMacSystemFont", "Segoe UI", "Roboto Condensed", "Source Sans Pro", Cantarell, Lato, Candara,"Segoe UI",  Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"',
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        color: 'rgba(0,0,0,0.7)!important',
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    botonSignin: {
        margin: theme.spacing(1, 2, 1),
    },
    botonSignup: {
        margin: theme.spacing(1, 2, 1),
    },
    spanOr: {
        margin: theme.spacing(1.2),
    },
    cajaBotones: {
        alignSelf: 'end',
    },
    switchLabel: {
        color: theme.palette.secondary.dark,
        fontWeight: '900 !important',
    },
    switchBase: {
        color: theme.palette.secondary.light,
        '&$checked': {
            color: theme.palette.secondary.dark,
        },
        '&$checked + $track': {
            backgroundColor: theme.palette.secondary.dark,
        },
    },
    checked: {},
    track: {},
}))

const Login = () => {
    const classes = useStyles()
    const router = useRouter()
    const db = new PouchDB('https://couch.codea.me/', { skip_setup: true })
    PouchDB.plugin(Authentication)

    const [username, setUsername] = useState('') // Ahora email
    const [password, setPassword] = useState('')
    const [avatar, setAvatar] = useState('')
    const [nombre, setNombre] = useState('')
    const [newUser, setNewUser] = useState(false)
    const [error, setError] = useState('')
    const [user, setUser] = useState('')
    const [loggedIn, setLoggedIn] = useState(false)
    const [roles, setRoles] = useState([])

    const handleUsernameChange = (event) => {
        setUsername(event.target.value)
    }

    const handlePasswordChange = (event) => {
        setPassword(event.target.value)
    }

    const handleNombreChange = (event) => {
        setNombre(event.target.value)
    }

    const handleAvatarChange = (event) => {
        setAvatar(event.target.value)
    }

    const handleSwitchChange = (event) => {
        setNewUser(event.target.checked)
    }

    const onKeyPressed = (event) => {
        if (event.key == 'Enter') {
            if (newUser) signup()
            else signin()
        }
    }

    // para redireccionar a / cuando el usuario ya esta autenticado
    useEffect(() => {
        if (user) {
            console.log('User signed in...')
            router.push('/')
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user])

    // signin process with couchDB

    const signin = () => {
        db.logIn(username, password)
            .then(function (response) {
                console.log('Response:', response)
                setLoggedIn(response.ok)
                setUser(response.name)
                setRoles(response.roles)
                setError('')
            })
            .catch(function (error) {
                console.log('ERROR SIGNIN:', error)
                let _error_ = ''
                switch (error.error) {
                    case 'unauthorized':
                        _error_ = '* '
                        break
                    default:
                        _error_ = error.error
                }
                switch (error.message) {
                    case 'Name or password is incorrect':
                        _error_ += 'Usuario o password incorrecto.'
                        break
                    default:
                        _error_ += error.message
                }

                setError(_error_)
                setLoggedIn(false)
                setUser('')
                setRoles([])
            })
    }

    // signup process with couchDB
    const signup = () => {
        // console.log('User:', username, ' Password:', password, ' Nombre:', nombre, 'Avatar:', avatar)
        db.signUp(username, password, {
            metadata: {
                email: username,
                nombre: nombre,
                avatar: avatar,
            },
        })
            .then(function (response) {
                console.log('Response SIGNUP:', response)
                return db.logIn(username, password)
            })
            .then(function (response) {
                console.log('RESPONSE LOGIN:', response)
                setLoggedIn(response.ok)
                setUser(response.name)
                setRoles(response.roles)
                setError('')
            })
            .catch(function (error) {
                console.log('ERROR SIGNUP:', error)
                let _error_ = ''
                switch (error.error) {
                    case 'unauthorized':
                        _error_ = '* '
                        break
                    default:
                        _error_ = error.error
                }
                switch (error.message) {
                    case 'Name or password is incorrect':
                        _error_ += 'Usuario o password incorrecto.'
                        break
                    case 'conflict':
                        _error_ += 'El nombre ya existe'
                    case 'forbidded':
                        _error_ += 'El nombre contiene caracteres inválidos'
                    default:
                        _error_ += error.message
                }

                setError(_error_)
                setLoggedIn(false)
                setUser('')
                setRoles([])
            })
    }

    if (typeof user === 'undefined' || !user) {
        return (
            <>
                <Head>
                    <title>Chronos</title>
                    <meta charSet='utf-8' />
                    <meta name='description' content='Chronos' />
                    <meta name='viewport' content='initial-scale=1.0, width=device-width' />
                    <link rel='icon' href='/favicon-32x32.png' />
                </Head>
                <ThemeProvider theme={temaChronosLight}>
                    <Container component='main' maxWidth='xl' className='main-container-signin'>
                        <CssBaseline />
                        <motion.div
                            positionTransition
                            initial={AnimateUp.initial}
                            animate={AnimateUp.animate}
                            exit={AnimateUp.exit}>
                            <div className={classes.centrado}>
                                <div className={classes.root}>
                                    <h2 className={classes.titulo}>Chronos</h2>

                                    <TextField
                                        className='flex-inner'
                                        size='small'
                                        id='email'
                                        name='email'
                                        placeholder='email'
                                        label='Email'
                                        aria-label='email'
                                        value={username}
                                        onChange={handleUsernameChange}
                                        onKeyPress={onKeyPressed}
                                        autoFocus
                                        autoComplete='off'
                                        required
                                        error={error !== ''}
                                    />
                                    <TextField
                                        className='flex-inner'
                                        size='small'
                                        id='password'
                                        name='password'
                                        placeholder='Enter Password'
                                        label='Password'
                                        type='password'
                                        aria-label='password'
                                        value={password}
                                        onChange={handlePasswordChange}
                                        onKeyPress={onKeyPressed}
                                        helperText={error}
                                        error={error !== ''}
                                        required
                                        autoComplete='off'
                                    />
                                    {newUser && (
                                        <>
                                            <TextField
                                                className='flex-inner'
                                                size='small'
                                                id='nombre'
                                                name='nombre'
                                                placeholder='Nombre'
                                                label='Nombre'
                                                aria-label='nombre'
                                                value={nombre}
                                                onChange={handleNombreChange}
                                                autoComplete='off'
                                            />
                                            <TextField
                                                className='flex-inner'
                                                size='small'
                                                id='avatar'
                                                name='avatar'
                                                placeholder='URL de tu Avatar/Foto'
                                                label='Avatar'
                                                aria-label='Avatar'
                                                value={avatar}
                                                onChange={handleAvatarChange}
                                                autoComplete='off'
                                            />
                                        </>
                                    )}
                                    <FormControlLabel
                                        className={classes.switchLabel}
                                        control={
                                            <Switch
                                                className={classes.switchBase}
                                                checked={newUser}
                                                onChange={handleSwitchChange}
                                                name='newUser'
                                                inputProps={{ 'aria-label': 'Soy nuevo, registrarme' }}
                                            />
                                        }
                                        label='Soy nuevo, registrarme!'
                                    />

                                    <Box className={classes.cajaBotones}>
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            onClick={newUser ? signup : signin}
                                            className={classes.botonSignin}>
                                            {newUser ? 'Registrarme' : 'Iniciar'}
                                        </Button>
                                    </Box>
                                </div>
                            </div>
                        </motion.div>
                    </Container>
                </ThemeProvider>
            </>
        )
    } else if (typeof user !== 'undefined' && user) {
        return (
            <>
                <div className='spinner'></div>
            </>
        )
    }
}

export default Login
