# Chronos, timekeeper for your projects.

_A timekeeper and task manager._

## Design

### User Interface

For the UI, material-ui was selected, which is rich and beautiful; and most users are used to it because of Android.
https://material-ui.com/

We used a template layout from https://github.com/mui-org/material-ui/blob/master/docs/src/pages/getting-started/templates/dashboard/Dashboard.js and modified to suite our like.

For User Avatars, we are using a service to get random user photos for use when user do not add one at registration.
https://pravatar.cc/

## Authentication

Using CouchDb in the server for saving data. With this we can use authentication with the couchdb server.
https://github.com/pouchdb-community/pouchdb-authentication

The remote CouchDB is configured to accept new users, and create a database per user.

The signup process creates the new user at the \_users database, and locks it for the user. Only the user and admins can read/write to this new per-user database.

### Database

The database used is CouchDb/PouchDb.
To add and get data, we use usePouch (https://christopher-astfalk.de/use-pouchdb/), which is a library that exposes hooks and an API.

The only frustration with this is that with Nextjs (SSR, SSG) it creates a local database at the server. It was impossible to use pouchdb-browser because of this (SSR). It does not work with dynamic component loading (which can be used to disable SSR for the component).

#### CouchDb Data Modeling

As in couchDb there are no collections (or tables), everything is in a single "collection of documents" called database.
So we have the Kronos database with its documents. Each document needs a model/type to be found or grouped when needed.

Also, each document must have an **_\_id_**, and be unique in all the database. This way couchDb can detect an edit on the same
document at the "same" time by two users, to make its magic with revisions. So we must, as recommended, generate the unique
_\_id_ for each document when created.

_We have these models_:

##### Users and Members

The uses are created when registered. The database is \_users and only can be accessed by the user itself and the administrators.
Members are users that are authorized to modify/view tasks in a project, the model is the same.

```json
{
    "email": "user@example.com",
    "name": "The username, equals to email",
    "nombre": "Nombre",
    "avatar": "shome_link_to_the_avatar",
    "roles": []
}
```

##### Customers

```json
{
    "_id": "some_unique_id",
    "model": "customers",
    "email": "email",
    "name": "The user name",
    "avatar": "url_to_image"
}
```

##### Projects

```json
{
    "_id": "Unique Project Name", // could be the project title
    "model": "projects",
    "created_by": "user@example.com",
    "team": "user1@example.com,user2@example.com",
    "customer": "some_customer_unique_id",
    "hourly_fee": "50.00"
}
```

##### Tasks

Each task can have a **_done_** field to indicate if the task is completed or not.
Each task can have a **_time_** field to indicate the total time spent in the task.
These time can be in many time-lapses, for example, one hour in a day, two the next day...
So, the **_created_at_** and **_finished_at_** are only the dates they where created and marked as done,
but it does not represent the whole time spent in the task.
**_sessions_** can serve for identifying the exact datetime lapses dedicated to the task.

```json
{
    "_id": "unique_task_title", // could be the task title
    "model": "tasks",
    "project_id": "unique_project_id",
    "time": "time in seconds: 3600", // TODO: update to db each minute
    "created_at": "a date time",
    "finished_at": "a date time",
    "sessions": [
        {
            "start": "2020/02/01 10:20",
            "end": "2020/02/01 11:48"
        },
        {
            "start": "2020/02/02 08:11",
            "end": "2020/02/02 14:19"
        }
        // ..
    ],
    "done": true
}
```

```
(C) 2020 by
Miguel Chavez Gamboa <miguel@codea.me>
https://www.gitlab.com/codeame/chronos
```
